﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Folha_de_Pagamento
{
    class FolhadePagamentoDTO
    {
        public int Id { get; set; }
        public string Funcionario { get; set; }
        public string Fornecedor { get; set; }
        public DateTime Pagamento { get; set; }
        public string FormadePagamento { get; set; }
        
    }
}
