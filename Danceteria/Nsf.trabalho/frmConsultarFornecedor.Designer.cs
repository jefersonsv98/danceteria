﻿namespace Nsf.trabalho
{
    partial class frmConsultarFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvFornecedor = new System.Windows.Forms.DataGridView();
            this.ID_Fornecedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_empresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_cnpj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_inscricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_rua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_bairro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_local = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_cep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_uf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFornecedor
            // 
            this.dgvFornecedor.AllowUserToAddRows = false;
            this.dgvFornecedor.AllowUserToDeleteRows = false;
            this.dgvFornecedor.BackgroundColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFornecedor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFornecedor.ColumnHeadersHeight = 40;
            this.dgvFornecedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Fornecedor,
            this.nm_empresa,
            this.nu_cnpj,
            this.nu_inscricao,
            this.nm_rua,
            this.nm_bairro,
            this.nu_local,
            this.nu_cep,
            this.nm_estado,
            this.ds_uf,
            this.nu_tel,
            this.ds_email,
            this.nm_funcionario});
            this.dgvFornecedor.Location = new System.Drawing.Point(-2, 157);
            this.dgvFornecedor.Name = "dgvFornecedor";
            this.dgvFornecedor.RowHeadersVisible = false;
            this.dgvFornecedor.Size = new System.Drawing.Size(767, 387);
            this.dgvFornecedor.TabIndex = 54;
            // 
            // ID_Fornecedor
            // 
            this.ID_Fornecedor.DataPropertyName = "ID_Fornecedor";
            this.ID_Fornecedor.HeaderText = "ID_Fornecedor";
            this.ID_Fornecedor.MinimumWidth = 10;
            this.ID_Fornecedor.Name = "ID_Fornecedor";
            this.ID_Fornecedor.Width = 150;
            // 
            // nm_empresa
            // 
            this.nm_empresa.DataPropertyName = "Nome da Empresa";
            this.nm_empresa.HeaderText = "Nome da Empresa";
            this.nm_empresa.Name = "nm_empresa";
            // 
            // nu_cnpj
            // 
            this.nu_cnpj.DataPropertyName = "CNPJ";
            this.nu_cnpj.HeaderText = "CNPJ";
            this.nu_cnpj.Name = "nu_cnpj";
            // 
            // nu_inscricao
            // 
            this.nu_inscricao.DataPropertyName = "Inscricao";
            this.nu_inscricao.HeaderText = "Inscricao";
            this.nu_inscricao.Name = "nu_inscricao";
            // 
            // nm_rua
            // 
            this.nm_rua.DataPropertyName = "Rua";
            this.nm_rua.HeaderText = "Rua";
            this.nm_rua.Name = "nm_rua";
            // 
            // nm_bairro
            // 
            this.nm_bairro.DataPropertyName = "Bairro";
            this.nm_bairro.HeaderText = "Bairro";
            this.nm_bairro.Name = "nm_bairro";
            // 
            // nu_local
            // 
            this.nu_local.DataPropertyName = "Local";
            this.nu_local.HeaderText = "Local";
            this.nu_local.Name = "nu_local";
            // 
            // nu_cep
            // 
            this.nu_cep.DataPropertyName = "CEP";
            this.nu_cep.HeaderText = "CEP";
            this.nu_cep.Name = "nu_cep";
            // 
            // nm_estado
            // 
            this.nm_estado.DataPropertyName = "Estado";
            this.nm_estado.HeaderText = "Estado";
            this.nm_estado.Name = "nm_estado";
            // 
            // ds_uf
            // 
            this.ds_uf.DataPropertyName = "UF";
            this.ds_uf.HeaderText = "UF";
            this.ds_uf.Name = "ds_uf";
            // 
            // nu_tel
            // 
            this.nu_tel.DataPropertyName = "Telefone";
            this.nu_tel.HeaderText = "Telefone";
            this.nu_tel.Name = "nu_tel";
            // 
            // ds_email
            // 
            this.ds_email.DataPropertyName = "Email";
            this.ds_email.HeaderText = "Email";
            this.ds_email.Name = "ds_email";
            // 
            // nm_funcionario
            // 
            this.nm_funcionario.DataPropertyName = "Funcionario";
            this.nm_funcionario.HeaderText = "Funcionario";
            this.nm_funcionario.Name = "nm_funcionario";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(563, 111);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(34, 26);
            this.button5.TabIndex = 53;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gold;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(603, 111);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 26);
            this.button4.TabIndex = 52;
            this.button4.Text = "Procurar";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(247, 111);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(282, 20);
            this.textBox1.TabIndex = 51;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(12, 108);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(208, 21);
            this.label15.TabIndex = 50;
            this.label15.Text = "Nome do Fornecedor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Jokerman", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(172, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(425, 55);
            this.label1.TabIndex = 126;
            this.label1.Text = "Consultar Fornecedor";
            // 
            // frmConsultarFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(767, 553);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvFornecedor);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarFornecedor";
            this.Text = "frmConsultarFornecedor";
            this.Load += new System.EventHandler(this.frmConsultarFornecedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFornecedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFornecedor;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Fornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_empresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_cnpj;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_inscricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_rua;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_bairro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_local;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_cep;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_uf;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_email;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_funcionario;
        private System.Windows.Forms.Label label1;
    }
}