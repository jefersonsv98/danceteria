﻿drop database if exists mar;

create database mar;

use mar;


CREATE TABLE `tb_cliente` (
	`ID_Cliente` int NOT NULL AUTO_INCREMENT,
	`nm_nome` varchar(25) NOT NULL,
	`ds_rg` varchar(50) NOT NULL,
	`ds_cpf` varchar(15) NOT NULL,
	`ds_sexo` varchar(20) NOT NULL,
	`dt_nascimento` DATE NOT NULL,
	`nr_telefone` varchar(15) NOT NULL,
	`nr_celular` varchar(50) NOT NULL,
	`ds_rua` varchar(45) NOT NULL,
	`ds_bairro` varchar(50) NOT NULL,
	`ds_cep` varchar(8) NOT NULL,
	ds_uf varchar(2) NOT NULL,
	`ds_numero` varchar(50) NOT NULL,
	PRIMARY KEY (`ID_Cliente`)
);

CREATE TABLE `tb_produto` (
	`ID_Produto` int NOT NULL AUTO_INCREMENT,
	`ds_produto` varchar(50) NOT NULL,
	`vl_preco` varchar(25) NOT NULL,
	PRIMARY KEY (`ID_Produto`)
);

CREATE TABLE `tb_funcionario` (
	`ID_Funcionario` int NOT NULL AUTO_INCREMENT,
	`ds_email` varchar(45) NOT NULL,
	`ds_senha` varchar(20) NOT NULL,
	`nm_nome` varchar(50) NOT NULL,
	`ds_cpf` varchar(15) NOT NULL,
	`ds_rg` varchar(15) NOT NULL,
	`ds_cargo` varchar(25) NOT NULL,
	`dt_nascimento` DATE NOT NULL,
	`nr_telefone` varchar(15) NOT NULL,
	`nr_celular` varchar(15) NOT NULL,
	`ds_rua` varchar(45) NOT NULL,
	`ds_numero` varchar(30) NOT NULL,
	`ds_bairro` varchar(45) NOT NULL,
	`ds_cep` varchar(45) NOT NULL,
	`ds_sexo` varchar(15) NOT NULL,
	`ds_admissao` DATETIME NOT NULL,
	`ds_permissao` bool NOT NULL,
	`ds_cidade` varchar(50) NOT NULL,
	`ds_estado` varchar(50) NOT NULL,
	PRIMARY KEY (`ID_Funcionario`)
);

CREATE TABLE `TB_Fornecedor` (
	`ID_Fornecedor` int NOT NULL AUTO_INCREMENT,
	`ds_denominacao` varchar(30) NOT NULL,
	`ds_end` varchar(45) NOT NULL,
	`ds_rua` varchar(45) NOT NULL,
	`ds_cidade` varchar(45) NOT NULL,
	`ds_estado` varchar(2) NOT NULL,
	`ds_inscricao` varchar(20) NOT NULL,
	`nr_tel` varchar(20) NOT NULL,
	`ds_ramo_atividade` varchar(45) NOT NULL,
	`ds_contato` varchar(45) NOT NULL,
	`ds_funcao` varchar(30) NOT NULL,
	`ds_prazo_entrega` varchar(15) NOT NULL,
	`ds_desconto` varchar(20) NOT NULL,
	PRIMARY KEY (`ID_Fornecedor`)
);

CREATE TABLE `tb_pedido` (
	`ID_Pedido` int NOT NULL AUTO_INCREMENT,
	`ID_Produto` int NOT NULL,
	`ID_Funcionario` int NOT NULL,
	`ID_Cliente` int NOT NULL,
	`fm_pagamento` varchar(50) NOT NULL,
	`qtd_produto` varchar(35) NOT NULL,
	`dt_data` DATE NOT NULL,
	`dt_horario` varchar(50) NOT NULL,
	PRIMARY KEY (`ID_Pedido`)
);

CREATE TABLE `TB_Estoque` (
	`ID_Estoque` int NOT NULL AUTO_INCREMENT,
	`ID_Produto` int NOT NULL,
	`qtd_produto` varchar(50) NOT NULL,
	PRIMARY KEY (`ID_Estoque`)
);

CREATE TABLE `tb_pagamento` (
	`ID_Pagamento` int NOT NULL AUTO_INCREMENT,
	`ID_Funcionario` int NOT NULL,
	`ID_Fornecedor` int NOT NULL,
	`ds_salario` varchar(50) NOT NULL,
	`dt_pagamento` DATE NOT NULL,
	PRIMARY KEY (`ID_Pagamento`)
);

CREATE TABLE `TB_Pedido_Item` (
	`ID_Pedido_Item` int NOT NULL AUTO_INCREMENT,
	`ID_Produto` int NOT NULL,
	`ID_Pedido` int NOT NULL,
	PRIMARY KEY (`ID_Pedido_Item`)
);

CREATE TABLE `tb_fluxo` (
	`ID_Fluxo` int NOT NULL AUTO_INCREMENT,
	`ID_Fornecedor` int NOT NULL,
	`ID_Funcionario` int NOT NULL,
	`ID_Produto` int NOT NULL,
	`nr_receber` varchar(50) NOT NULL,
	`nr_pagar` varchar(50) NOT NULL,
	`nr_entrada` varchar(50) NOT NULL,
	`nr_saida` varchar(50) NOT NULL,
	PRIMARY KEY (`ID_Fluxo`)
);

ALTER TABLE `tb_pedido` ADD CONSTRAINT `tb_pedido_fk0` FOREIGN KEY (`ID_Produto`) REFERENCES `tb_produto`(`ID_Produto`);

ALTER TABLE `tb_pedido` ADD CONSTRAINT `tb_pedido_fk1` FOREIGN KEY (`ID_Funcionario`) REFERENCES `tb_funcionario`(`ID_Funcionario`);

ALTER TABLE `tb_pedido` ADD CONSTRAINT `tb_pedido_fk2` FOREIGN KEY (`ID_Cliente`) REFERENCES `tb_cliente`(`ID_Cliente`);

ALTER TABLE `TB_Estoque` ADD CONSTRAINT `TB_Estoque_fk0` FOREIGN KEY (`ID_Produto`) REFERENCES `tb_produto`(`ID_Produto`);

ALTER TABLE `tb_pagamento` ADD CONSTRAINT `tb_pagamento_fk0` FOREIGN KEY (`ID_Funcionario`) REFERENCES `tb_funcionario`(`ID_Funcionario`);

ALTER TABLE `tb_pagamento` ADD CONSTRAINT `tb_pagamento_fk1` FOREIGN KEY (`ID_Fornecedor`) REFERENCES `TB_Fornecedor`(`ID_Fornecedor`);

ALTER TABLE `TB_Pedido_Item` ADD CONSTRAINT `TB_Pedido_Item_fk0` FOREIGN KEY (`ID_Produto`) REFERENCES `tb_produto`(`ID_Produto`);

ALTER TABLE `TB_Pedido_Item` ADD CONSTRAINT `TB_Pedido_Item_fk1` FOREIGN KEY (`ID_Pedido`) REFERENCES `tb_pedido`(`ID_Pedido`);

ALTER TABLE `tb_fluxo` ADD CONSTRAINT `tb_fluxo_fk0` FOREIGN KEY (`ID_Fornecedor`) REFERENCES `TB_Fornecedor`(`ID_Fornecedor`);

ALTER TABLE `tb_fluxo` ADD CONSTRAINT `tb_fluxo_fk1` FOREIGN KEY (`ID_Funcionario`) REFERENCES `tb_funcionario`(`ID_Funcionario`);

ALTER TABLE `tb_fluxo` ADD CONSTRAINT `tb_fluxo_fk2` FOREIGN KEY (`ID_Produto`) REFERENCES `tb_produto`(`ID_Produto`);
