﻿namespace Nsf.trabalho
{
    partial class frmConsultarFluxodeCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvFluxodeCaixa = new System.Windows.Forms.DataGridView();
            this.id_fluxo_caixa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_pago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_fornecedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFluxodeCaixa)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFluxodeCaixa
            // 
            this.dgvFluxodeCaixa.AllowUserToAddRows = false;
            this.dgvFluxodeCaixa.AllowUserToDeleteRows = false;
            this.dgvFluxodeCaixa.BackgroundColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFluxodeCaixa.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFluxodeCaixa.ColumnHeadersHeight = 40;
            this.dgvFluxodeCaixa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_fluxo_caixa,
            this.vl_pago,
            this.vl_total,
            this.vl_fornecedor});
            this.dgvFluxodeCaixa.Location = new System.Drawing.Point(-8, 154);
            this.dgvFluxodeCaixa.Name = "dgvFluxodeCaixa";
            this.dgvFluxodeCaixa.RowHeadersVisible = false;
            this.dgvFluxodeCaixa.Size = new System.Drawing.Size(539, 323);
            this.dgvFluxodeCaixa.TabIndex = 54;
            // 
            // id_fluxo_caixa
            // 
            this.id_fluxo_caixa.DataPropertyName = "ID";
            this.id_fluxo_caixa.HeaderText = "ID";
            this.id_fluxo_caixa.Name = "id_fluxo_caixa";
            this.id_fluxo_caixa.Width = 150;
            // 
            // vl_pago
            // 
            this.vl_pago.DataPropertyName = "Valor Pago";
            this.vl_pago.HeaderText = "Valor Pago";
            this.vl_pago.Name = "vl_pago";
            // 
            // vl_total
            // 
            this.vl_total.DataPropertyName = "Valor Total";
            this.vl_total.HeaderText = "Valor Total";
            this.vl_total.Name = "vl_total";
            // 
            // vl_fornecedor
            // 
            this.vl_fornecedor.DataPropertyName = "Valor do Fornecedor";
            this.vl_fornecedor.HeaderText = "Valor do Fornecedor";
            this.vl_fornecedor.Name = "vl_fornecedor";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(399, 112);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(31, 26);
            this.button5.TabIndex = 53;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gold;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(436, 112);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 26);
            this.button4.TabIndex = 52;
            this.button4.Text = "Procurar";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(154, 112);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(228, 20);
            this.textBox1.TabIndex = 51;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(16, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 21);
            this.label15.TabIndex = 50;
            this.label15.Text = "ID do Caixa";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Jokerman", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(125, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 55);
            this.label1.TabIndex = 126;
            this.label1.Text = "Fluxo de Caixa";
            // 
            // frmConsultarFluxodeCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(531, 485);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvFluxodeCaixa);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarFluxodeCaixa";
            this.Text = "frmConsultarFluxodeCaixa";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFluxodeCaixa)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFluxodeCaixa;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_fluxo_caixa;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_total;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_fornecedor;
    }
}