﻿using Nsf.trabalho.BD_Login_class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmCadastrarFuncionario : Form
    {
        public frmCadastrarFuncionario()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmMenu telapedido = new frmMenu();

            string nome = txtnome.Text;
            string senha = txtsenha.Text;

            if (nome == string.Empty || nome == "Nome Completo")
            {
                MessageBox.Show("O campo nome é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            if (senha == string.Empty || senha == "Password")
            {
                MessageBox.Show("O campo senha é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            loginDTO dados = new loginDTO();
            dados.nome = nome;
            dados.senha = senha;
            LoginBusiness salvardados = new LoginBusiness();
            salvardados.Salvar(dados);

            MessageBox.Show("Cadastrado");

            
            frmlogar voltar = new frmlogar();
            this.Hide();
            voltar.ShowDialog();
            

        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox4_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void frmCadastrarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click_1(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            frmlogar voltar = new frmlogar();
            this.Hide();
            voltar.ShowDialog();
        }

        private void lblclose1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
